
terraform {
  required_providers {
    virtualbox = {
      source = "shekeriev/virtualbox"
      version = "0.0.4"
    }
  }
}
provider virtualbox {}

module "Masters" {
  source = "./modules"
  nameVar = "master"
  countVar  = 3
  //currentStatusVar = "poweroff"
}

module "Slaves" {
  source = "./modules"
  nameVar = "Slave"
  countVar = 2
  //currentStatusVar = "poweroff"
}
output "ipsMasters" {
  value = module.Masters.ipv4_addressOUT
}
output "ipsSlaves" {
  value = module.Slaves.ipv4_addressOUT
}

