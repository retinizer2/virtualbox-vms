
variable "nameVar" {
  description = "Name for VM"
  type        = string
  default     = "Master"
}
variable "countVar" {
  description = "count of VMs in group"
  type        = number
  default     = 3
}
variable "memoryVar" {
  description = "RAM value"
  type        = string
  default     = "3072 mib"
}

variable "currentStatusVar" {
  default = "running"
}
variable "interface_name" {
  description = "Name of host_interface network"
  type        = string
  default     = "enp0s20u3"
}