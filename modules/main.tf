
terraform {
  required_providers {
    virtualbox = {
      source = "shekeriev/virtualbox"
      version = "0.0.4"
    }
  }
}

resource "virtualbox_vm" "node" {
    count = var.countVar
    name = "${format("%s-%02d", var.nameVar, count.index+1)}"
    image = "https://app.vagrantup.com/ubuntu/boxes/xenial64/versions/20190507.0.0/providers/virtualbox.box"  
    cpus = 1
    memory = var.memoryVar
    status = var.currentStatusVar

     network_adapter {
       type = "bridged"
       host_interface= var.interface_name
    }

    lifecycle {
      ignore_changes = [
        memory
      ]
      }
}